<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title> Green Campus UDRU</title>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/pe-icons.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

	<link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/hand-holding.ico'); ?>" />
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			'use strict';
			jQuery('body').backstretch([
				"<?php echo base_url(); ?>assets/images/bg/bg1.jpg",
				"<?php echo base_url(); ?>assets/images/bg/bg2.jpg",
				"<?php echo base_url(); ?>assets/images/bg/bg3.jpg"
				], {
					duration: 5000,
					fade: 500,
					centeredY: true
				});

			$("#mapwrapper").gMap({
				controls: false,
				scrollwheel: false,
				markers: [{
					latitude: 40.7566,
					longitude: -73.9863,
					icon: {
						image: "<?php echo base_url(); ?>assets/images/marker.png",
						iconsize: [44, 44],
						iconanchor: [12, 46],
						infowindowanchor: [12, 0]
					}
				}],
				icon: {
					image: "<?php echo base_url(); ?>assets/images/marker.png",
					iconsize: [26, 46],
					iconanchor: [12, 46],
					infowindowanchor: [12, 0]
				},
				latitude: 40.7566,
				longitude: -73.9863,
				zoom: 14
			});
		});
	</script>
</head>
<!--/head-->

<body>
	<header class="navbar navbar-inverse navbar-fixed-top " role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>">
					<h1><span class="fa fa-leaf" style="color:green;"></span> Green Campus UDRU</h1>
				</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url(); ?>assets/index.html">Home</a></li>
					<li><a href="<?php echo base_url(); ?>assets/about-us.html">About Us</a></li>
					<li><a href="<?php echo base_url(); ?>assets/services.html">Services</a></li>
					<li><a href="<?php echo base_url(); ?>assets/portfolio.html">Portfolio</a></li>
					<li><a href="<?php echo base_url(); ?>assets/blog.html">Blog</a></li>
					<li><a href="<?php echo base_url(); ?>assets/contact-us.html">Contact</a></li>
					<li class="dropdown active">
						<a href="<?php echo base_url(); ?>assets/#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="icon-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>assets/project-item.html">Project Single</a></li>
							<li><a href="<?php echo base_url(); ?>assets/blog-item.html">Blog Single</a></li>
							<li class="active"><a href="<?php echo base_url(); ?>assets/404.html">404</a></li>
						</ul>
					</li>
					<li><span class="search-trigger"><i class="fa fa-search"></i></span></li>
				</ul>
			</div>
		</div>
	</header>
	<!--/header-->
	<div class="gap"></div>

	<section id="main-slider" class="no-margin">
		<div class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="carousel-content center centered">
									<!-- <span class="home-icon pe-7s-leaf"></span><br> -->
									<div class="gap"></div>
									<h2 class="boxed animation animated-item-1 fade-down" style="color:green;">
										<span class="fa fa-leaf" style="color:green;"></span> Green Campus UDRU
									</h2>
									<br>
									<h3 class="boxed animation animated-item-2 fade-up" style="color:yellow;"> <b>มหาวิทยาลัยสีเขียว </b></h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/.item-->
			</div>
			<!--/.carousel-inner-->
		</div>
		<!--/.carousel-->
	</section>
	<!--/#main-slider-->

	<div id="content-wrapper">
		<section id="#" class="bg-white" style="background: #fff;">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="center gap fade-down section-heading">
							<h2 class="main-title">ระบบโครงสร้างพื้นฐาน </h2>
							<hr>
							<p>พื้นที่มหาวิทยาลัยทั้งหมด 2096 ไร่ 3354065 ตารางเมตร พื้นที่รอบมหาวิทยาลัยมีความยาวทั้งหมด 9 กิโลเมตร</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 fade-up" style="background: #e6e6e6;">
						<div class="col-sm-7">
							<img src="<?php echo base_url(); ?>assets/images/imgcontent/areaAll.jpg" class="img-responsive" alt="">
						</div>
						<div class="col-sm-5 bg-light">
							<br>
							<p><b>พื้นที่มหาวิทยาลัยทั้งหมด</b></p>
							<p>พื้นที่มหาวิทยาลัยทั้งหมด 2096 ไร่ 3354065 ตารางเมตร พื้นที่รอบมหาวิทยาลัยมีความยาวทั้งหมด 9 กิโลเมตร
							</p>
						</div>
					</div>
					<div class="col-sm-12 fade-down" style="background: #fff;">
						<div class="col-sm-5 bg-light">
							<br>
							<p><b>พื้นที่อาคารทั้งหมด</b></p>
							<p>พื้นที่อาคารทั้งหมด  ≈ 90 ไร่ 145326 ตารางเมตร พื้นที่รอบอาคารมีความยาวทั้งหมด 5.68 กิโลเมตร
							</p>
						</div>
						<div class="col-sm-7">
							<img src="<?php echo base_url(); ?>assets/images/imgcontent/areaBulding.jpg" class="img-responsive" alt="">
						</div>
					</div>
					<div class="col-sm-12 fade-up" style="background: #e6e6e6;">
						<div class="col-sm-7">
							<img src="<?php echo base_url(); ?>assets/images/imgcontent/areaforest.jpg" class="img-responsive" alt="">
						</div>
						<div class="col-sm-5 bg-white">
							<br>
							<p><b>พื้นที่ป่า</b></p>
							<p>มีพื้นที่รวมทั้งหมด 1049 ไร่ 1681389 ตารางเมตร มีพื้นที่ความยาวทั้งหมด 15.48 กิโลเมตร
							</p>
						</div>
					</div>
					<div class="col-sm-12 fade-down" style="background: #fff;">
						<div class="col-sm-5 bg-light">
							<br>
							<p><b>พื้นที่รองรับน้ำ</b></p>
							<p>มีพื้นที่รวมทั้งหมด 38 ไร่ 60838 ตารางเมตร มีพื้นที่ความยาว 2.09 กิโลเมตร
							</p>
						</div>
						<div class="col-sm-7">
							<img src="<?php echo base_url(); ?>assets/images/imgcontent/areaWater.jpg" class="img-responsive" alt="">
						</div>
					</div>
					<div class="col-sm-12 fade-down" style="background: #e6e6e6;">
						<div class="col-sm-7">
							<img src="<?php echo base_url(); ?>assets/images/imgcontent/areabenefit.jpg" class="img-responsive" alt="">
						</div>
						<div class="col-sm-5 bg-light">
							<br>
							<p><b>พื้นที่การใช้ประโยชน์</b></p>
							<p>มีพื้นที่ทั้งหมด 8 ไร่ 12718 ตารางเมตร มีพื้นที่ความยาว 0.87 กิโลเมตร
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="services" class="white" >
			<div class="container">
				<!-- <div class="gap"></div> -->
				<div class="row">
					<div class="col-md-12">
						<div class="center gap fade-down section-heading">
							<h2 class="main-title">ระบบโครงสร้างพื้นฐาน </h2>
							<hr>
							<p>พื้นที่มหาวิทยาลัยทั้งหมด 2096 ไร่ 3354065 ตารางเมตร พื้นที่รอบมหาวิทยาลัยมีความยาวทั้งหมด 9 กิโลเมตร</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-camera fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">Photography</h3>
								<p>Nay middleton him admitting consulted and behaviour son household. Recurred advanced he oh together entrance speedily suitable. Ready tried gay state fat could boy its among shall.</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-thumbs-o-up fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">Marketing</h3>
								<p>Unfeeling agreeable suffering it on smallness newspaper be. So come must time no as. Do on unpleasing possession as of unreserved.</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-ticket fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">Event Management</h3>
								<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
				</div>
				<!--/.row-->
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-star fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">Star Gazing</h3>
								<p>Yet joy exquisite put sometimes enjoyment perpetual now. Behind lovers eat having length horses vanity say had its</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-cogs fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">Software Support</h3>
								<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
					<div class="col-md-4 col-sm-6">
						<div class="service-block">
							<div class="pull-left bounce-in">
								<i class="fa fa-google-plus fa fa-md"></i>
							</div>
							<div class="media-body fade-up">
								<h3 class="media-heading">SEO Services</h3>
								<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
							</div>
						</div>
					</div>
					<!--/.col-md-4-->
				</div>
				<!--/.row-->
			</div>
			<div class="gap"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="center gap fade-down section-heading">
							<h2 class="main-title">Our Skills</h2>
							<hr>
							<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="tile-progress tile-red bounce-in">
							<div class="tile-header">
								<h3>Video Editing</h3>
								<span>Our cutting room floor is messy.</span>
							</div>
							<div class="tile-progressbar">
								<span data-fill="65.5%" style="white: 65.5%;"></span>
							</div>
							<div class="tile-footer">
								<h4>
									<span class="pct-counter counter">65.5</span>%
								</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-progress tile-cyan bounce-in">
							<div class="tile-header">
								<h3>Marketing</h3>
								<span>How well we can sell you and your brand.</span>
							</div>
							<div class="tile-progressbar">
								<span data-fill="98.5%" style="white: 98.5%;"></span>
							</div>
							<div class="tile-footer">
								<h4>
									<span class="pct-counter counter">98.5</span>%
								</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-progress tile-primary bounce-in">
							<div class="tile-header">
								<h3>Web Development</h3>
								<span>We love servers and stuff.</span>
							</div>
							<div class="tile-progressbar">
								<span data-fill="90%" style="white: 90%;"></span>
							</div>
							<div class="tile-footer">
								<h4>
									<span class="pct-counter counter">90</span>%
								</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-progress tile-pink bounce-in">
							<div class="tile-header">
								<h3>Coffee</h3>
								<span>We done make good joe, though.</span>
							</div>
							<div class="tile-progressbar">
								<span data-fill="10%" style="white: 10%;"></span>
							</div>
							<div class="tile-footer">
								<h4>
									<span class="pct-counter counter">10</span>%
								</h4>
							</div>
						</div>
					</div>
				</div>
				<!-- 	/.row -->
			</div>
		</section>


		<!-- <section id="single-quote" class="divider-section">
			<div class="container">
				<div class="gap"></div>
				<div class="row">
					<div class='col-md-offset-2 col-md-8 fade-up'>
						<div class="carousel slide" data-ride="carousel" id="quote-carousel">
							<div class="carousel-inner">
								<div class="item active">
									<blockquote>
										<div class="row">
											<div class="col-sm-4 text-center">
												<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/imgcontent/img1.jpg" style="white: 500px;height:200px;">
											</div>
											<div class="col-sm-8">
												<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
												<small>Someone famous</small>
											</div>
										</div>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="gap"></div>
			</div>
		</section> -->

		<section id="about-us" class="white">
			<div class="container">
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="center gap fade-down section-heading">
							<h2 class="main-title">A Little About Us</h2>
							<hr>
							<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1 fade-up">
						<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan
							ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in
							erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut
						aliquam massa nisl quis neque. Suspendisse in orci enim.</p>

						<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan
							ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in
							erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut
						aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
					</div>
					<div class="col-md-4 fade-up">

					</div>
				</div>
				<div class="gap"></div>
				<div class="row fade-up">
					<div class="col-md-6">
						<div class="testimonial-list-item">
							<img class="pull-left img-responsive quote-author-list" src="<?php echo base_url(); ?>assets/images/team/team01.jpg">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<small>Manager at <cite title="Source Title">Company</cite></small>
							</blockquote>
						</div>
						<div class="testimonial-list-item">
							<img class="pull-left img-responsive quote-author-list" src="<?php echo base_url(); ?>assets/images/team/team01.jpg">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<small>Manager at <cite title="Source Title">Company</cite></small>
							</blockquote>
						</div>
					</div>
					<div class="col-md-6">
						<div class="testimonial-list-item">
							<img class="pull-left img-responsive quote-author-list" src="<?php echo base_url(); ?>assets/images/team/team01.jpg">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<small>Manager at <cite title="Source Title">Company</cite></small>
							</blockquote>
						</div>
						<div class="testimonial-list-item">
							<img class="pull-left img-responsive quote-author-list" src="<?php echo base_url(); ?>assets/images/team/team01.jpg">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<small>Manager at <cite title="Source Title">Company</cite></small>
							</blockquote>
						</div>
					</div>
				</div>

				<div class="gap"></div>
				<div class="center gap fade-down section-heading">
					<h2 class="main-title">Meet The Team</h2>
					<hr>
					<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
				</div>
				<div class="gap"></div>

				<div id="meet-the-team" class="row">
					<div class="col-md-3 col-xs-6">
						<div class="center team-member">
							<div class="team-image">
								<img class="img-responsive img-thumbnail bounce-in" src="<?php echo base_url(); ?>assets/images/team/team01.jpg" alt="">
								<div class="overlay">
									<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/team/team01.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="team-content fade-up">
								<h5>Daniel Jones<small class="role muted">Web Design</small></h5>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
								<a class="btn btn-social btn-facebook" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-google-plus"></i></a>                                <a class="btn btn-social btn-twitter" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-xs-6">
						<div class="center team-member">
							<div class="team-image">
								<img class="img-responsive img-thumbnail bounce-in" src="<?php echo base_url(); ?>assets/images/team/team02.jpg" alt="">
								<div class="overlay">
									<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/team/team02.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="team-content fade-up">
								<h5>John Smith<small class="role muted">Marketing Director</small></h5>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
								<a class="btn btn-social btn-facebook" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-google-plus"></i></a>                                <a class="btn btn-social btn-twitter" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="center team-member">
							<div class="team-image">
								<img class="img-responsive img-thumbnail bounce-in" src="<?php echo base_url(); ?>assets/images/team/team03.jpg" alt="">
								<div class="overlay">
									<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/team/team03.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="team-content fade-up">
								<h5>Dave Gorman<small class="role muted">Web Design</small></h5>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
								<a class="btn btn-social btn-facebook" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-google-plus"></i></a>                                <a class="btn btn-social btn-twitter" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="center team-member">
							<div class="team-image">
								<img class="img-responsive img-thumbnail bounce-in" src="<?php echo base_url(); ?>assets/images/team/team04.jpg" alt="">
								<div class="overlay">
									<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/team/team04.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="team-content fade-up">
								<h5>Steve Smith<small class="role muted">Sales Assistant</small></h5>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
								<a class="btn btn-social btn-facebook" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-google-plus"></i></a>                                <a class="btn btn-social btn-twitter" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!--/#meet-the-team-->
                <!-- <div class="gap"></div>
                	<div class="gap"></div> -->
                </div>
              </section>

        <!-- <section id="stats" class="divider-section">
            <div class="gap"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-xs-6">
                        <div class="center bounce-in">
                            <span class="stat-icon"><span class="pe-7s-timer bounce-in"></span></span>
                            <h1><span class="counter">246000</span></h1>
                            <h3>HOURS SAVED</h3>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="center bounce-in">
                            <span class="stat-icon"><span class="pe-7s-light bounce-in"></span></span>
                            <h1><span class="counter">16875</span></h1>
                            <h3>FRESH IDEAS</h3>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="center bounce-in">
                            <span class="stat-icon"><span class="pe-7s-graph1 bounce-in"></span></span>
                            <h1><span class="counter">99999999</span></h1>
                            <h3>HUGE PROFIT</h3>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="center bounce-in">
                            <span class="stat-icon"><span class="pe-7s-box2 bounce-in"></span></span>
                            <h1><span class="counter">54875</span></h1>
                            <h3>THINGS IN BOXES</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gap"></div>
          </section> -->

          <section id="portfolio" class="white">
          	<div class="container">
          		<!-- <div class="gap"></div> -->
          		<div class="center gap fade-down section-heading">
          			<h2 class="main-title">Examples Of Excellence</h2>
          			<hr>
          			<p>She evil face fine calm have now. Separate screened he outweigh of distance landlord.</p>
          		</div>
          		<ul class="portfolio-filter fade-down center">
          			<li><a class="btn btn-outlined btn-primary active" href="<?php echo base_url(); ?>assets/#" data-filter="*">All</a></li>
          			<li><a class="btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#" data-filter=".apps">Apps</a></li>
          			<li><a class="btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#" data-filter=".nature">Nature</a></li>
          			<li><a class="btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#" data-filter=".design">Design</a></li>
          		</ul>
          		<!--/#portfolio-filter-->

          		<ul class="portfolio-items col-3 isotope fade-up">
          			<li class="portfolio-item apps isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio01.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio01.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item joomla nature isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio02.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio02.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item bootstrap design isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio03.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio03.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item joomla design apps isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio04.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio04.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item joomla apps isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio05.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio05.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item wordpress nature isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio06.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio06.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item joomla design apps isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio07.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio07.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item joomla nature isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio08.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio08.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          			<li class="portfolio-item wordpress design isotope-item">
          				<div class="item-inner">
          					<img src="<?php echo base_url(); ?>assets/images/portfolio/folio09.jpg" alt="">
          					<h5>Portfolio Project</h5>
          					<div class="overlay">
          						<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/images/portfolio/folio09.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
          					</div>
          				</div>
          			</li>
          			<!--/.portfolio-item-->
          		</ul>
          	</div>
          </section>

          <section id="testimonial-carousel" class="divider-section">
          	<div class="gap"></div>
          	<div class="container">
          		<div class="row">
          			<div class="center gap fade-down section-heading">
          				<h2 class="main-title">What They Have Been Saying</h2>
          				<hr>
          				<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
          				<div class="gap"></div>
          			</div>
          			<div class='col-md-offset-2 col-md-8 fade-up'>
          				<div class="carousel slide" data-ride="carousel" id="quote-carousel">
          					<!-- Bottom Carousel Indicators -->
          					<ol class="carousel-indicators">
          						<li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
          						<li data-target="#quote-carousel" data-slide-to="1"></li>
          						<li data-target="#quote-carousel" data-slide-to="2"></li>
          					</ol>
          					<!-- Carousel Slides / Quotes -->
          					<div class="carousel-inner">
          						<!-- Quote 1 -->
          						<div class="item active">
          							<blockquote>
          								<div class="row">
          									<div class="col-sm-3 text-center">
          										<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/team/team01.jpg" style="width: 100px;height:100px;">
          									</div>
          									<div class="col-sm-9">
          										<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
          										<small>Someone famous</small>
          									</div>
          								</div>
          							</blockquote>
          						</div>
          						<!-- Quote 2 -->
          						<div class="item">
          							<blockquote>
          								<div class="row">
          									<div class="col-sm-3 text-center">
          										<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/team/team02.jpg" style="width: 100px;height:100px;">
          									</div>
          									<div class="col-sm-9">
          										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
          										<small>Someone famous</small>
          									</div>
          								</div>
          							</blockquote>
          						</div>
          						<!-- Quote 3 -->
          						<div class="item">
          							<blockquote>
          								<div class="row">
          									<div class="col-sm-3 text-center">
          										<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/team/team03.jpg" style="width: 100px;height:100px;">
          									</div>
          									<div class="col-sm-9">
          										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
          										<small>Someone famous</small>
          									</div>
          								</div>
          							</blockquote>
          						</div>
          					</div>
          				</div>
          			</div>
          		</div>
          		<div class="gap"></div>
          	</div>
          </section>

          <section id="blog" class="white">
          	<div class="container">
          		<div class="center gap fade-down section-heading">
          			<div class="gap"></div>
          			<h2 class="main-title">From The Blog</h2>
          			<hr>
          			<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
          		</div>
          		<div class="gap"></div>
          		<div class="row">
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio02.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio06.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio11.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          		</div>
          		<div class="gap"></div>
          		<div class="row">
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio02.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio06.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          			<div class="col-md-4">
          				<div class="post">
          					<div class="post-img-content">
          						<img src="<?php echo base_url(); ?>assets/images/portfolio/folio11.jpg" class="img-responsive" />
          						<div class="overlay">
          							<a class="preview btn btn-outlined btn-primary" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-link"></i></a>
          						</div>
          					</div>
          					<div class="content">
          						<h2 class="post-title">Post Title</h2>
          						<div class="author">
          							<i class="fa fa-user"></i> By <b>Author</b> | <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">April 11th, 2014</time>
          						</div>
          						<div>
          							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          						</div>
          						<div class="read-more-wrapper">
          							<a href="<?php echo base_url(); ?>assets/#" class="btn btn-outlined btn-primary">Read more</a>
          						</div>
          					</div>
          				</div>
          			</div>
          		</div>
          	</div>
          </section>

        <!-- <div id="mapwrapper">
            <div id="map"></div>
          </div> -->

          <section id="contact" class="white">
          	<div class="container">
          		<!-- <div class="gap"></div> -->
          		<div class="center gap fade-down section-heading">
          			<h2 class="main-title">Get In Touch</h2>
          			<hr>
          			<p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
          		</div>
          		<div class="gap"></div>
          		<div class="row">
          			<div class="col-md-4 fade-up">
          				<h3>Contact Information</h3>
          				<p><span class="icon icon-home"></span>Time Square, New York<br/>
          					<span class="icon icon-phone"></span>+36 65984 405<br/>
          					<span class="icon icon-mobile"></span>+36 65984 405<br/>
          					<span class="icon icon-envelop"></span> <a href="<?php echo base_url(); ?>assets/#">email@infinityteam.com</a> <br/>
          					<span class="icon icon-twitter"></span> <a href="<?php echo base_url(); ?>assets/#">@infinityteam.com</a> <br/>
          					<span class="icon icon-facebook"></span> <a href="<?php echo base_url(); ?>assets/#">Infinity Agency</a> <br/>
          				</p>
          			</div>
          			<!-- col -->

          			<div class="col-md-8 fade-up">
          				<h3>Drop Us A Message</h3>
          				<br>
          				<br>
          				<div id="message"></div>
          				<form method="post" action="sendemail.php" id="contactform">
          					<input type="text" name="name" id="name" placeholder="Name" />
          					<input type="text" name="email" id="email" placeholder="Email" />
          					<input type="text" name="website" id="website" placeholder="Website" />
          					<textarea name="comments" id="comments" placeholder="Comments"></textarea>
          					<input class="btn btn-outlined btn-primary" type="submit" name="submit" value="Submit" />
          				</form>
          			</div>
          			<!-- col -->
          		</div>
          		<!-- row -->
          		<div class="gap"></div>
          	</div>
          </section>
        </div>

        <div id="footer-wrapper">
        	<section id="bottom" class="">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-3 col-sm-6 about-us-widget">
        					<h4>Global Coverage</h4>
        					<p>Was drawing natural fat respect husband. An as noisy an offer drawn blush place. These tried for way joy wrote witty. In mr began music weeks after at begin.</p>
        				</div>
        				<!--/.col-md-3-->

        				<div class="col-md-3 col-sm-6">
        					<h4>Company</h4>
        					<div>
        						<ul class="arrow">
        							<li><a href="<?php echo base_url(); ?>assets/#">Company Overview</a></li>
        							<li><a href="<?php echo base_url(); ?>assets/#">Meet The Team</a></li>
        							<li><a href="<?php echo base_url(); ?>assets/#">Our Awesome Partners</a></li>
        							<li><a href="<?php echo base_url(); ?>assets/#">Our Services</a></li>
        						</ul>
        					</div>
        				</div>
        				<!--/.col-md-3-->

        				<div class="col-md-3 col-sm-6">
        					<h4>Latest Articles</h4>
        					<div>
        						<div class="media">
        							<div class="pull-left">
        								<img class="widget-img" src="<?php echo base_url(); ?>assets/images/portfolio/folio01.jpg" alt="">
        							</div>
        							<div class="media-body">
        								<span class="media-heading"><a href="<?php echo base_url(); ?>assets/#">Blog Post A</a></span>
        								<small class="muted">Posted 14 April 2014</small>
        							</div>
        						</div>
        						<div class="media">
        							<div class="pull-left">
        								<img class="widget-img" src="<?php echo base_url(); ?>assets/images/portfolio/folio02.jpg" alt="">
        							</div>
        							<div class="media-body">
        								<span class="media-heading"><a href="<?php echo base_url(); ?>assets/#">Blog Post B</a></span>
        								<small class="muted">Posted 14 April 2014</small>
        							</div>
        						</div>
        					</div>
        				</div>
        				<!--/.col-md-3-->

        				<div class="col-md-3 col-sm-6">
        					<h4>Come See Us</h4>
        					<address>
        						<strong>Ace Towers</strong><br>
        						New York Ave,<br>
        						New York, 215648<br>
        						<abbr title="Phone"><i class="fa fa-phone"></i></abbr> 546 840654 05
        					</address>
        				</div>
        				<!--/.col-md-3-->
        			</div>
        		</div>
        	</section>
        	<!--/#bottom-->

        	<footer id="footer" class="">
        		<div class="container">
        			<div class="row">
        				<div class="col-sm-6">
        					&copy; 2018 Green Campus UDRU .
        				</div>
        				<div class="col-sm-6">
        					<ul class="pull-right">
        						<li><a id="gototop" class="gototop" href="<?php echo base_url(); ?>assets/#"><i class="fa fa-chevron-up"></i></a></li>
        						<!--#gototop-->
        					</ul>
        				</div>
        			</div>
        		</div>
        	</footer>
        	<!--/#footer-->
        </div>


        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWDPCiH080dNCTYC-uprmLOn2mt2BMSUk&amp;sensor=true"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
      </body>

      </html>